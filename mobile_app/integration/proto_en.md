The key words **"MUST"**, **"MUST NOT"**, **"REQUIRED"**, **"SHALL"**, **"SHALL NOT"**, **"SHOULD"**, **"SHOULD NOT"**, **"RECOMMENDED"**,  **"MAY"**, and **"OPTIONAL"** in this document are to be interpreted as described in RFC 2119.

System architecture
===================

Point of service (self service car wash)
----------------------------------------
Any car wash consists of several (one or more) washing posts, typically between 4 and 8. It may also optionally have several vacuum cleaner posts (typically 4 to 10).

An unique URL-prefix is assigned to every car wash in the system (e.g., `https://fingw.profiwash.com.ua:8443/object/215/` )

Payment processing service
--------------------------
A car wash use 3rd party services to accept payments for washing. Any such service is identified with an unique ASCII string denoted `psp_id`. This document describes protocol between Payment Processing Service (HTTP client) and Self Service Car Wash (HTTP-service).

Communication protocol
======================

General notes
-------------
We use HTTP as an underlying protocol and RESTful approach.

Payment Processing Service is a client in this protocol, and Point of Service (Self Service Car Wash) is a server.

Request parameters may be passed:
 - in **URL path** (e.g., `http://host/prefix/{some_parameter}/action`)
 - in **URL query** (e.g., `http://host/prefix/action?{parameter_name}={parameter_value}`)
 - in **request body** in JSON format (e.g., `{"parameter_name"="parameter_value"}`)

If any parameter mentioned isn't included to the path, it should be passed in request body (for `POST` and `PUT` HTTP methods) or in query (other HTTP methods). Optional parameters that are passed in URL query or request body may be ommited. Optional parameters that are passed in URI path may only be ommited if they are the last part of the path (e.g., `prefix/action[/{parameter}]`), but may be replaced with wildcard asterisk ( \* ) or dash ( - ) character otherwise (e.g. `prefix/*/*/{timestamp}` or `prefix/-/price/{number}`).

All time-valued parameters (except for timestamp) are expressed in seconds, timestamp values are expressed in milliseconds.

Return format is JSON. Response to any request **SHALL** include following fields:
 - `status` - request success / failure. If not defined otherwise, may be `ok` or `error`.
 - `error_message` (only with `status=error`) - UTF8 string with error message.
 - `contents` (required if `status=ok`) - other data returned with response.

If not mentioned otherwise, all strings are ASCII.

Authentication and Authorization
--------------------------------

Data transferred between parties is protected with:
 - transport level security (TLS)
 - application level security measures.

Server must use a proper SSL/TLS certificate for connections, valdated with Certificate Authority (CA) and bound to host name used in URL prefix.

All subsequent part of this document describes only application level security measures.

Three levels of security are used:
 - No authentication: used for data that is not sensitive and doesn't lead to financial commitments. No application-level authentication & authorization measures are used.
 - Bearer JSON Web Token (JWT): used for potentially sensitive data that doesn't lead to financial commitments. A Bearer authorization token is provided for client requests with HTTP headers. The token is JWT digital signed with client's private key. No authorization is used in server responses.
 - JSON Web Signature (JWS): used for claims that create, modify or withdraw financial commitments. Claims are digitally signed with issuer's key.

Both client requests and server responses are subject to authorization, if required by protocol.

Depending on Self Service Car Wash and Payment Processing Service agreements, application level security measures may be relaxed. Typically, this approach is used when both client and server are owned by a single legal entity. Ih this case, only Transport Level Security is used, and Application level security requirements are ignored.

### Keys and algorithms
No symmetric key encryption or MACs are used in this protocol. Both server and client MUST support JWS algorithms as follows: ES256, ES384, ES512, RS256, RS368, RS512. (See RFC 7518: JSON Web Algorithms (JWA)) 

### Certificates
Both client and server MUST use their own private/public key pair. Before using this protocol, parties MUST exchange with X.509 certificates providing their respective public keys and validate them. Way of certificate validation is out of scope of this document. Both client and server certificate MAY NOT be the same certificate that used in TLS, and MAY be self-signed (i.e. not signed with CA). Client certificate MUST be registered in a server software. Server key SHOULD be registered in a client software. Below, client & server singing certificates are presumed registered and validated.

### Bearer JSON Web Token (JWT) Authorization
Client MUST provide a JWT signed with Payment Processing Service, as a HTTP Bearer Auth token.

JWT header parameters:
  - "alg" (algorithm) MUST be present, as required by JWT specification
  - at least one of "jku" (JWK URL), "x5u" (X.509 URL), "kid" (Key ID), "x5t" (X.509 Certificate SHA-1 Thumbprint), "x5t#S256" (X.509 Certificate SHA-256 Thumbprint) field SHOULD be present. Using "x5t#S256" parameter is RECOMMENDED.

JWT claims (payload):
  - "iss" (issuer): MUST be "psp:" || `psp_id` (where  "||" is string concatenation)
  - "aud" (audience): MUST be a string equal to car wash URL prefix, or a JSON array including this string
  - "exp" (expiration time): MUST be present, SHOULD be no more than 24 hours after JWT creation time. Note: if token is used on a third party device, RECOMMENDED expiration time is within 1 hour since creation.
  - "nbf" (not before): RECOMMENDED
  - "jti" (JWT ID): RECOMMENDED

Server SHOULD check given JWT before processing a request unless application level security is relaxed.

No additional protection is applied to server responses in this auth category.

### JSON Web Signature (JWS) Authorization
Request body MUST be a JSON Web Signature in a JWS JSON Serialization format.

JWS payload MUST be a JSON object:
  - it MUST include request parameters that are not present in URL path or query
  - it MUST include request parameters marked as "protected" even if they are already present in the request URL
  - it MUST include a "url_prefix" field set to the URL prefix value associated with Self Service Car Wash
  - it MUST include a "psp_id" field set to the `psp_id` identifier associated with Payment Processing Service
  - it's RECOMMENDED to include all request parameters to the JWS payload.

JWS header parameters:
  - "alg" MUST be present, as required by JWS specification
  - at least one of "jku" (JWK URL), "x5u" (X.509 URL), "kid" (Key ID), "x5t" (X.509 Certificate SHA-1 Thumbprint), "x5t#S256" (X.509 Certificate SHA-256 Thumbprint) field SHOULD be present. Using "x5t#S256" parameter is RECOMMENDED.
  - "cty" is RECOMMENDED and SHOULD BE set to "JSON" or "application/JSON"

Server MUST check signature before processing a request unless application level security is relaxed.

Response body MUST be a JSON Web Signature in a JSW JSON Serialization format.

JWS payload in response MUST be a JSON object, and MUST include all response fields specified in this document. Additionally, it MUST include both `psp_id` and `url_prefix` values passed in request.

JWS header parameters:
  - "alg" MUST be present, as required by JWS specification
  - at least one of "jku" (JWK URL), "x5u" (X.509 URL), "kid" (Key ID), "x5t" (X.509 Certificate SHA-1 Thumbprint), "x5t#S256" (X.509 Certificate SHA-256 Thumbprint) field SHOULD be present. Using "x5t#S256" and "x5u" parameters is RECOMMENDED.
  - "cty" is RECOMMENDED and SHOULD BE set to "JSON" or "application/JSON"

Client SHOULD check signature before processing a response unless application level security is relaxed.

Requests
--------

GET _{prefix}_/meta
-----------------
**Car wash generic information**

 - Authorization: none
 - Parameters: none
 - Response:
   - `id` (string) - car wash identifier
   - `name` (UTF8 string) - name of the car wash shown to mobile app users
   - `city` (UTF8 string) - city / town / village where car wash is located
   - `address` (UTF8 string) - address in the city
   - `location` (string) - GPS coordinates (WGS84 latitude & longitude) in degrees, formatted as xx.xxxxx,yy.yyyyy (negative numbers mean south / western hemisphere)
   - `wash_count` (positive integer) - number of car wash posts
   - `vc_count` (non-negative integer) - number of vacuum cleaner posts

Returns generic information about car wash. Typically, values returned are not subject to change except for \*\_count values.

GET _{prefix}_/security/get_x509/{thumbprint}
--------------------------------------------
 - Authorization: none
 - Parameters:
   - `thumbprint` - HEX string (':' and '-' characters are allowed as separators), SHA-1 or SHA-256 thumbprint of the certificate.
 - Response: X.509 certificate file as-is (without JSON wrapping/encoding)

GET _{prefix}_/equip/_{equip}_/_{number}_/status
------------------------------------
**Car wash equipment state**

 - Authorization: recommended, Bearer JWT
 - Parameters:
  - `equip` _( `wash` | `vc` )_ (optional) - equipment type (wash or vacuum cleaner)
  - `number` (positive integer) (optional) - car wash post number / vacuum cleaner post number (1-based)
 - Response:
  - `equip` _( `wash` | `vc` )_ - equipment type (wash or vacuum cleaner)
  - `number` (positive integer) - car wash post number / vacuum cleaner post number (1-based)
  - `state` - _( `ready` | `occupied` | `unavailable` | `closed` )_

State of the car wash / vacuum cleaner post.

 - `ready` - ready to accept payment
 - `occupied` - in use just now, ready to accept payments
 - `unavailable` - not available due to technical reasons (e.g. network connectivity error).
 - `closed` - not available due to non-technical reasons (switched off by personnel, non working hours etc).

If both `equip` and `number` parameters are set, then response `contents` is a JSON object. If at least one of `equip` and `number` isn't specified (passed as \* or - ), `contents` is JSON array of object, each object describin one of posts. In this case, `equip` and `number` values in every object of response **MUST** be present.

It is RECOMMENDED to make this request before initiating any payment. It will allow to "fail fast" and don't start a payment that can't be delivered to the equipment.

It is also RECOMMENDED to use Bearer JWT authentication, as it may uncover some possible payment issues that are specific to the Payment Processing Service.

GET _{prefix}_/equip/_{equip}_/price[/_{number}_]
-----------------
**Current prices**

 - Authorization: required, Bearer JWT
 - Parameters:
  - `equip` _( `wash` | `vc` )_ (optional) - equipment type (wash or vacuum cleaner)
  - `number` (non-negative integer) (optional) - washing (cleaning) more number
 - Response:
  - `equip` _( `wash` | `vc` )_ - equipment type (wash or vacuum cleaner)
  - `number` - washing (cleaning) more number
  - `label` (string) - mode label
  - `name` (UTF8 string) - mode name (shown to mobile app users)
  - `price` (non-negative integer) - current mode price in cents per minute
  - `valid_for` (positive integer) (optional) - duration of time when price is expected to be valid

If both `equip` and `number` parameters are set, then response `contents` is a JSON object. If at least one of `equip` and `number` isn't specified (passed as \* or - or omitted), `contents` is JSON array of object, each object describin one of washing modes. In this case, `equip` and `number` values in response **MUST** be present.

PUT _{prefix}_/equip/_{equip}_/_{number}_/transaction/_{timestamp}_
-----------------------------------------
**Credit money to equipment**

 - Authorization: required, JWS
 - Parameters:
  - `equip` _( `wash` | `vc` )_ (protected) - equipment type (wash or vacuum cleaner)
  - `number` (positive integer) (protected) - car wash post number / vacuum cleaner post number (1-based)
  - `amount` (positive integer) (protected) - amount of money expressed in cents
  - `currency` (string) (protected) - only "UAH" is allowed
  - `timestamp` (64 bit integer) (protected) - UNIX timestamp \* 1000 (milliseconds since 00:00 1 Jan 1970 UTC). Should be within +/-300000 (5 minutes) around server current time, otherwise server **SHOULD** discard an operation. **MUST** be unique within any "payment service provider - car wash" pair. This is required  even for different `equip` / `number` values. If two or more operations are performed in the same millisecond, timestamps of all but one of them **MUST** be adjusted by client for 1 or more milliseconds to achieve uniqueness.
  - `time_to_wait` (integer) (optional) - recommended duration limit for a request. **MUST NOT** be less than 5 seconds. If after this time server still can't give final result for the transation, it **SHOULD** return `state=processing`. After this, transaction state may be followed with `GET {prefix}/equip/*/*/transaction/{timestamp}` request.
  - `payment_tag` (UTF8 string) (optional) (protected) - arbitrary string that identifies payment. Isn't processed by server (apart from logging / storage), may be used for debug purposes.
 - Response:
  - `state` _( `processing` | `completed` | `rejected` )_

Credit self-service equipment with money.

Client **SHOULD** send transactions in ascending `timestamp` order. Expection is for concurrent requests.

Client **MUST NOT** send transaction with `timestamp` value covered with any previous `DELETE .../equip/{equip}/{number}/transaction/{timestamp}` request.

Server **MUST** assure idempotence of the request. In other words, when request is performed more than once, second and further invocations must not change server state. If request is performed for second or more time, server **MAY** return error and **MUST** return `state` value as in `GET...` request. Server **SHOULD** send response no later than `time_to_wait` seconds after request, even if tranaction final state isn't known at the time.

If client can't get response (because of network timeout, 4xx HTTP code from proxy server, or for any other reason), client **SHOULD** repeat request until it get response. Client **SHOULD NOT** perform `GET` and/or `DELETE` requests for this transaction before getting `state` response from `PUT` request.

`state` values are:
 - `completed`: when payment is succesfully transferred to the targer equipment.
 - `rejected`: when server can't perform request.
 - `processing`: when transaction final state isn't known yet.

DELETE _{prefix}_/equip/_{equip}_/_{number}_/transaction/_{timestamp}_
--------------------------------------------------------
**Acknowledge final transaction status receiving**

 - Authorization: required, Bearer JWT
 - Parameters:
  - `equip` _( `wash` | `vc` )_ (optional) - equipment type (wash or vacuum cleaner)
  - `number` (positive integer) (optional) - car wash post number / vacuum cleaner post number (1-based)
  - `timestamp` (64-bit integer with an optional '<=' prefix) - transaction(s) timestamp
 - Response: none

When client recieves final transaction status, it may acknowledge it ("delete" transaction from server for purpose of this protocol). After this request, server **MAY** not return the transaction anymore.

Timestamp may designate a single value (integer number) or a range of values (an integer number with "<=" prefix, '<' character should be URL-encoded, e.g. as %3c). The former is referred to as **acknowledgement of a single operation** , while the latter is referred to as **acknowledgement of a range**. For _acknowledgement of a range_, request applies to all transactions with `timestamp` value that is less or equal to given value.

Client **SHOULD** perform _acknowledgement of a range_ with wildcard-valued `equip` and `number` at least once in 24 hours, if any operation occured at all during this time.

Server **MUST** return an error and ignore a request if a transaction with a given timestamp isn't finished yet (i.e. its `state=processing`). If a unfinished transaction is covered by _acknowledgement of a range_ request, server **MUST NOT** exclude the transaction from the further processing, and also **MUST NOT** exclude any transaction with timestamp higher that an unfinished one has. Server **MAY** exclude transaction with lower timestamps from further processing.

Server **MUST** assure idempotence of the request. In other words, after successful performance of the request, second and further invocations must not change server state.

GET _{prefix}_/equip/_{equip}_/_{number}_/transaction[/_{timestamp}_]
-------------------------------------------------------
**Retrieve transaction status**

 - Request authorization: required, Bearer JWT
 - Response authorization: required, JWS
 - Parameters:
  - `equip` _( `wash` | `vc` )_ (optional) - equipment type (wash or vacuum cleaner)
  - `number` (positive integer) (optional) - car wash post number / vacuum cleaner post number (1-based)
  - `timestamp` (64-bit integer with an optional '<=' prefix) - transaction(s) timestamp
  - `max_count` (positive integer) - soft limit for number of returned objects
 - Response:
 - `state` _( `processing` | `completed` | `rejected` )_ - current state of the transaction
  - `amount`
  - `currency`
  - `timestamp`
  - `payment_tag` - parameters set at tx creation time

Returns status of the transaction(s).

If `timestamp` parameter is set, response's `contents` is a JSON object. Otherwise, it's a JSON array of objects. This remains in effect even if only one transaction is selected.
`max_count` parameter **MUST NOT** be set if `timestamp` parameter is set.

Client **MUST NOT** use `timestamp` value of transaction that was subject to any previous `DELETE .../equip/{equip}/{number}/transaction/{timestamp}` request.

If `timestamp` value is not set in request (i.e., either void or passed as \* or -), server **SHOULD NOT** return transaction which were subject to any preceding `DELETE...` request. Server **MUST** return at least one transaction that weren't subject to preceding `DELETE...` request if such transaction exists at all. Server **SHOULD** return no more than `max_count` records (i.e. objects in array). If server returns two transactions with respective timestamps `t1` < `t2`, it also **MUST** return any transaction with timestamp `t` which satisfies `t1` < `t` < `t2` and that weren't subject to any `DELETE...` request. In other words, list of unacknoledged transaction should fully cover some continuous range of timestamps. Server **SHOULD** return transactions in ascending `timestamp` order. Server **MAY** return only part of transactions that satisfy request requirements.

If any previous `GET` or `PUT` request returned final value (i.e., `finished` or `rejected`) for `state` field, server **MUST** return the same value in any subsequent response.
